package lecture1;

public class UserService {
    public User createUser(String firstName, String secondName, String number, String bankDetails) {
        User user = new User();
        user.setBankDetails(bankDetails);
        user.setNumber(number);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        return user;
    }
}
