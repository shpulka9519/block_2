package lecture1;

public class User {
    private String firstName;
    private String secondName;
    private String number;
    private String bankDetails;

    public void setBankDetails(String bankDetails) {
        this.bankDetails = bankDetails;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getBankDetails() {
        return bankDetails;
    }

    public String getNumber() {
        return number;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getFirstName() {
        return firstName;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", number='" + number + '\'' +
                ", bankDetails='" + bankDetails + '\'' +
                '}';
    }
}
